package com.example.parsing.service;

public interface ParseService {

    String parseXml();

    String parseJson();

}
