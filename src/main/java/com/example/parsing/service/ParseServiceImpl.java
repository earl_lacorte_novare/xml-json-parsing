package com.example.parsing.service;

import com.example.parsing.model.Food;
import com.example.parsing.model.Pet;
import com.example.parsing.model.Pets;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import org.springframework.util.CollectionUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

public class ParseServiceImpl implements ParseService {

    @Override
    public String parseXml() {
        String message;
        StringBuilder stringBuilder = new StringBuilder();

        try {
            File file = new File("src//main//resources//sample.xml");

            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(file);
            document.getDocumentElement().normalize();

            stringBuilder.append("Root element: ").append(document.getDocumentElement().getNodeName()).append("\n");
            NodeList nodeList = document.getElementsByTagName("book");

            for (int i = 0; i < nodeList.getLength(); i++) {
                Node node = nodeList.item(i);

                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;

                    stringBuilder.append("\nBook ").append(i + 1).append(":\n");
                    stringBuilder.append("Category: ").append(element.getAttribute("category")).append("\n");
                    stringBuilder.append("Title: ").append(element.getElementsByTagName("title").item(0).getTextContent()).append("\n");

                    int authorCount = element.getElementsByTagName("author").getLength();
                    for (int x = 0; x < authorCount; x++) {
                        stringBuilder.append("Author ").append(x + 1).append(": ").append(element.getElementsByTagName("author").item(x).getTextContent()).append("\n");
                    }

                    stringBuilder.append("Year: ").append(element.getElementsByTagName("year").item(0).getTextContent()).append("\n");
                    stringBuilder.append("Price: ").append(element.getElementsByTagName("price").item(0).getTextContent()).append("\n");
                }
            }

            message = stringBuilder.toString();
        } catch (SAXException | ParserConfigurationException | IOException e) {
            message = e.getMessage();
        }

        return message;
    }

    @Override
    public String parseJson() {
        StringBuilder stringBuilder = new StringBuilder();
        String message;

        try (JsonReader reader = new JsonReader(new FileReader("src//main//resources//sample.json"))) {
            Gson gson = new GsonBuilder().create();
            Pets pets = gson.fromJson(reader, Pets.class);

            stringBuilder.append("Pet List:\n");

            List<Pet> petList = pets.getPets();
            for (int i = 0; i < petList.size(); i++) {
                Pet pet = petList.get(i);

                stringBuilder.append("\n");
                stringBuilder.append("Pet ").append(i + 1).append(":\n");
                stringBuilder.append("Name: ").append(pet.getName()).append("\n");
                stringBuilder.append("Species: ").append(pet.getSpecies()).append("\n");
                stringBuilder.append("Birth Year: ").append(pet.getBirthYear()).append("\n");

                List<Food> petFood = pet.getFood();
                if (CollectionUtils.isEmpty(petFood)) {
                    stringBuilder.append("Food: none\n");
                } else {
                    for (int x = 0; x < petFood.size(); x++) {
                        stringBuilder.append("Food ").append(x + 1).append(": ").append(petFood.get(x).getName()).append("\n");
                    }
                }
            }

            message = stringBuilder.toString();
        } catch (IOException e) {
            message = e.getMessage();
        }

        return message;
    }
}