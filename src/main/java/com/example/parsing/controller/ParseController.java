package com.example.parsing.controller;

import com.example.parsing.service.ParseServiceImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/parse")
public class ParseController {

    private ParseServiceImpl service = new ParseServiceImpl();

    @GetMapping(value = "/xml")
    public String parseXml() {
        return service.parseXml();
    }

    @GetMapping(value = "/json")
    public String parseJson() {
        return service.parseJson();
    }
}
